package main

import (
	"net/http"

	"github.com/robopuff/gopubsub/pub/internal/config"
	"github.com/robopuff/gopubsub/pub/internal/logger"
	"github.com/robopuff/gopubsub/pub/internal/producer"
	app "github.com/robopuff/gopubsub/pub/internal/server"
)

var hash, version string

func main() {
	cfg, err := config.Load()
	if err != nil {
		logger.Group("server").WithError(err).Panic("cannot load config")
	}

	rmq := producer.NewRabbitMQProducer(cfg)
	if err = rmq.Connect(); err != nil {
		logger.Group("server").WithError(err).Panic("cannot establish connection to rabbitmq")
	}
	defer rmq.Close()

	mux := http.NewServeMux()
	mux.HandleFunc("/health", app.NewHealthHandler(version, hash).Handle)
	mux.HandleFunc("/ws", app.NewWebsocketHandler(rmq).Handle)

	srv, err := app.NewServer(cfg, mux)
	if err != nil {
		logger.Group("server").WithError(err).Panic("cannot create new HTTP server")
	}

	logger.Group("server").Infof("starting http server at :%d", cfg.Http.Bind)
	err = srv.ListenAndServe()
	logger.Group("server").WithError(err).Panic("error listening http server")
}