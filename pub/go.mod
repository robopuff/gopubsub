module github.com/robopuff/gopubsub/pub

go 1.16

require (
	github.com/gorilla/websocket v1.4.2
	github.com/ilyakaznacheev/cleanenv v1.2.5
	github.com/sirupsen/logrus v1.8.1
	github.com/streadway/amqp v1.0.0
	github.com/stretchr/testify v1.6.1
	github.com/wagslane/go-rabbitmq v0.6.2
)
