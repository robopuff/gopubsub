# HTTP server (publisher)

This part creates HTTP server which have Websocket endpoint.
It will publish whatever comes to it, into RabbitMQ queue.

## Running

```shell
$ go mod vendors
$ docker-compose up -d rabbitmq
$ make run
```

## Config (dotenv)

```dotenv
HTTP_BIND = 8080
RABBITMQ_ADDRESS = localhost:5672
RABBITMQ_QUEUE = pubsubdemo
```

## Design choices

### Libraries

* `gorilla/websocket` for easier and more compliant websockets
* `ilyakaznacheev/cleanenv` for config
* `sirupsen/logrus` nice logs
* `stretchr/testify` testing and assertions
* `wagslane/go-rabbitmq` a `streadway/amqp` wrapper

### Logs

Logging by default is outputed to stdout in JSON format, it's easier
that way to collect them to some system-level logger, like DataDog

### Server

There are two endpoints, `/health` to get health information
(important when working with *k8s* or similar) and `/ws` for
websocket connection. It will accept eveyrthing without any
difference and send it to rabbitMQ - no limit on message size
by design but in real life there should be one - don't trust 
the end-user to respect your limitations, limit the user :)