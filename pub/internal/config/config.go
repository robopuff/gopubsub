package config

import (
	"fmt"

	"github.com/ilyakaznacheev/cleanenv"

	"github.com/robopuff/gopubsub/pub/internal/logger"
)

type Config struct {
	Http Http
	RabbitMQ RabbitMQ
}

type Http struct {
	Bind int `env:"HTTP_BIND" env-default:"8080"`
}

type RabbitMQ struct {
	Address string `env:"RABBITMQ_ADDRESS" env-default:"localhost:5672"`
	Queue string `env:"RABBITMQ_QUEUE" env-default:"pubsubdemo"`
}

func Load() (*Config, error) {
	cfg := &Config{}
	err := cleanenv.ReadConfig(".env", cfg)
	if err == nil {
		return cfg, nil
	}

	logger.Group("config").WithError(err).Info("reading config file `.env` failed, continuing with env variables")
	if err := cleanenv.ReadEnv(cfg); err != nil {
		return cfg, fmt.Errorf("error reading environment variables: %v", err)
	}

	return cfg, nil
}