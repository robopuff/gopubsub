// +build integration

package server_test

import (
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHealthResponse(t *testing.T) {
	res, err := http.Get("http://localhost:8080/health")
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	// Check basic headers
	assert.Equal(t, http.StatusOK, res.StatusCode)
	assert.Equal(t, "application/json", res.Header.Get("Content-Type"))

	bodyBytes, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}
	bodyString := string(bodyBytes)

	// Check the response body is what we expect.
	assert.Contains(t, bodyString, `"host":`)
	assert.Contains(t, bodyString, `"version":`)
	assert.Contains(t, bodyString, `"hash":`)
}