// +build integration

package server_test

import (
	"testing"

	"github.com/gorilla/websocket"
	"github.com/streadway/amqp"
	"github.com/stretchr/testify/assert"
	"github.com/wagslane/go-rabbitmq"
)

func TestWebsocket(t *testing.T) {
	// Connect to the server
	ws, _, err := websocket.DefaultDialer.Dial("ws://localhost:8080/ws", nil)
	if err != nil {
		t.Fatalf("%v", err)
	}
	defer ws.Close()

	for i := 0; i < 20; i++ {
		if err := ws.WriteMessage(websocket.TextMessage, []byte("my-message")); err != nil {
			t.Fatalf("%v", err)
		}
		_, p, err := ws.ReadMessage()
		if err != nil {
			t.Fatalf("%v", err)
		}

		assert.Equal(t, "produced message with `my-message`", string(p))
	}
}

func TestWebsocketProduceMessage(t *testing.T) {
	if err := purgeQueue(); err != nil {
		t.Fatal(err)
	}

	consumer, err := rabbitmq.NewConsumer("amqp://localhost:5672/", amqp.Config{})
	defer consumer.Disconnect()
	consumer.StartConsuming(
		func(d rabbitmq.Delivery) bool {
			assert.Equal(t, "plain/text", d.ContentType)
			assert.Equal(t, []byte("hello"), d.Body)
			return true
		},
		"pubsubdemo",
		[]string{"pubsubdemo"},
	)

	// Connect to the server
	ws, _, err := websocket.DefaultDialer.Dial("ws://localhost:8080/ws", nil)
	if err != nil {
		t.Fatalf("%v", err)
	}
	defer ws.Close()

	if err := ws.WriteMessage(websocket.TextMessage, []byte("hello")); err != nil {
		t.Fatalf("%v", err)
	}
}

func TestWebsocketProduceUniqueIDMessage(t *testing.T) {
	if err := purgeQueue(); err != nil {
		t.Fatal(err)
	}

	consumer, err := rabbitmq.NewConsumer("amqp://localhost:5672/", amqp.Config{})
	defer consumer.Disconnect()
	consumer.StartConsuming(
		func(d rabbitmq.Delivery) bool {
			assert.Equal(t, "plain/text", d.ContentType)
			assert.NotEqual(t, []byte("id"), d.Body)
			assert.NotEmpty(t, d.Body)
			return true
		},
		"pubsubdemo",
		[]string{"pubsubdemo"},
	)

	// Connect to the server
	ws, _, err := websocket.DefaultDialer.Dial("ws://localhost:8080/ws", nil)
	if err != nil {
		t.Fatalf("%v", err)
	}
	defer ws.Close()

	if err := ws.WriteMessage(websocket.TextMessage, []byte("id")); err != nil {
		t.Fatalf("%v", err)
	}
}

func purgeQueue() error {
	conn, err := amqp.Dial("amqp://localhost:5672/")
	if err != nil {
		return err
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		return err
	}
	defer ch.Close()

	qd, err := ch.QueueDeclare(
		"pubsubdemo", false, false, false, false, nil,
		)
	if err != nil {
		return err
	}

	_, err = ch.QueuePurge(qd.Name, false)
	return err
}