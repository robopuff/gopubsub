package server

import (
	"fmt"
	"net/http"

	"github.com/gorilla/websocket"

	"github.com/robopuff/gopubsub/pub/internal/logger"
	"github.com/robopuff/gopubsub/pub/internal/producer"
)

type wsHandler struct {
	RabbitMQ producer.Producer
}

// NewWebsocketHandler create new instance of the handler
func NewWebsocketHandler(rmq producer.Producer) *wsHandler {
	return &wsHandler{rmq}
}

// Handle establishes websocket connection
func (h *wsHandler) Handle(w http.ResponseWriter, r *http.Request) {
	u := websocket.Upgrader{
		ReadBufferSize: 1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(*http.Request) bool {
			// don't check origin - not safe but good enough for it right now
			return true
		},
		Error: func(w http.ResponseWriter, r *http.Request, status int, reason error) {
			logger.Group("websocket").WithError(reason).Errorf("websocket error %d", status)
		},
	}
	c, err := u.Upgrade(w, r, w.Header())
	if err != nil {
		logger.Group("websocket").WithError(err).Error("websocket upgrade errored")
		http.Error(w, "could not open websocket connection", http.StatusBadRequest)
		return
	}

	defer c.Close()
	for {
		mt, m, err := c.ReadMessage()
		if err != nil {
			logger.Group("websocket").WithError(err).Error("websocket read error")
			break
		}

		h.RabbitMQ.Send(m)
		logger.Group("websocket").Infof("received message [%d]: `%s`", mt, m)
		err = c.WriteMessage(mt, []byte(fmt.Sprintf("produced message with `%s`", m)))
		if err != nil {
			logger.Group("websocket").WithError(err).Error("websocket write error")
			break
		}
	}
}

