package server

import (
	"encoding/json"
	"net/http"
	"os"

	"github.com/sirupsen/logrus"
)

// Response JSON formatted health response struct
type Response struct {
	Host string `json:"host"`
	Version string `json:"version"`
	Hash string `json:"hash"`
}

type healthHandler struct {
	version, hash string
}

// NewHealthHandler create new instance of the handler
func NewHealthHandler(version, hash string) *healthHandler {
	return &healthHandler{version, hash}
}

// Handle creates health response
func (h *healthHandler) Handle(w http.ResponseWriter, r *http.Request) {
	// set CORS headers
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "OPTIONS,POST")
	w.Header().Set("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, X-Country, X-Store, X-Section, X-Wishlist, X-Tracking-Variables, X-Geo")
	if r.Method == http.MethodOptions {
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if r.Method != http.MethodGet {
		http.Error(w, "Allowed methods: GET", http.StatusMethodNotAllowed)
		return
	}

	host, _ := os.Hostname()
	if err := json.NewEncoder(w).Encode(Response{
		host,
		h.version,
		h.hash,
	}); err != nil {
		logrus.WithError(err).Error("health query errored")
	}
}