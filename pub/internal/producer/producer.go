package producer

// Producer RabbitMQ connection interface
type Producer interface {
	// Connect creates connection
	Connect() error

	// Send sends message to the queue defined in config
	Send(message []byte)

	// Close closes connection
	Close()
}
