package producer

import (
	"fmt"

	"github.com/streadway/amqp"
	"github.com/wagslane/go-rabbitmq"

	"github.com/robopuff/gopubsub/pub/internal/config"
	"github.com/robopuff/gopubsub/pub/internal/logger"
)

// NewRabbitMQProducer creates new instance of rabbitmq connection
func NewRabbitMQProducer(cfg *config.Config) Producer {
	return &conn{
		cfg: &cfg.RabbitMQ,
	}
}

type conn struct {
	cfg       *config.RabbitMQ
	publisher rabbitmq.Publisher
}

// Connect creates connection to RabbitMQ
// This should be refactored to be more reliable and support reconnection
func (c *conn) Connect() error {
	logger.Group("rabbitmq").WithField("address", c.cfg.Address).Info("trying to connect to rabbitmq")
	publisher, returns, err := rabbitmq.NewPublisher(fmt.Sprintf("amqp://%s/", c.cfg.Address), amqp.Config{})
	if err != nil {
		return err
	}

	c.publisher = publisher
	go func() {
		for r := range returns {
			logger.Group("rabbitmq").Infof("message returned from server: %s", string(r.Body))
		}
	}()
	return nil
}

// Close closes connection to rabbitmq
func (c *conn) Close() {
	c.publisher.StopPublishing()
}

// Send sends message to rabbitmq queue defined in config
func (c *conn) Send(message []byte) {
	if err := c.publisher.Publish(
		message,
		[]string{c.cfg.Queue},
		rabbitmq.WithPublishOptionsContentType("plain/text"),
	); err != nil {
		logger.Group("rabbitmq").WithError(err).Error("error publishing message to rabbitmq")
	}
}
