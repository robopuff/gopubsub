package logger

import (
	"os"

	"github.com/sirupsen/logrus"
)

// Group sets an specific group of traces
func Group(group string) *logrus.Entry {
	logrus.SetFormatter(&logrus.JSONFormatter{})

	h, _ := os.Hostname()

	return logrus.WithFields(logrus.Fields{
		"group":    group,
		"hostname": h,
	})
}
