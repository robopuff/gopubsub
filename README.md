# gopubsup

This project is made out of 3 parts,

1. **pub** Websocket server that listens for incoming messages and propagates them into rabbitMQ
2. **sub** Listens to rabbitMQ and propagates whatever comes to connected clients via websocket
3. **client** demo client written in *go* with *tview*

Each project is self-sustainable and in reality would be a separate repository. Please check
corresponding `README.md` files for more part-specific instructions and explanations.

In overall I had quite a little time to do it but I think I've managed to make something workable,
tests need more work definitely.

## Requirements

* *Docker*
* *Golang* >= 1.16

## How to run

To simply run whole stack, just use:

```shell
$ make run
```

In order to rebuild the docker images, use:

```shell
$ make build
```

## Todo

* Tests with `godog` for all features/services
* Better integration tests for `pub` and `sub`
* Test `rabbitMQ` producer & consumer
* More reliable `client` with better logging

## Things that didn't make it

* `godog` tests
* gRPC connection between `pub` and `sub` so only one websocket server is needed
