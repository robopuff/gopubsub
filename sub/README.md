# RabbitMQ consumer

This part creates RabbitMQ consumer.
It will consume whatever comes from RabbitMQ queue.

## Running

```shell
$ go mod vendors
$ docker-compose up -d rabbitmq
$ make run
```

## Config (dotenv)

```dotenv
HTTP_BIND = 8090
RABBITMQ_ADDRESS = localhost:5672
RABBITMQ_QUEUE = pubsubdemo
```

## Design choices

### Libraries

Same as pub part (see [pub/README.md](../pub/README.md#Libraries))

### Codebase

Some parts of the code here should be exported to another module
(especially config, logger, http server, health endpoint) so we
don't copy and reuse the same line of codes over and over again.

### Logs

Same as pub part (see [pub/README.md](../pub/README.md#Logs))

### Server

There are two endpoints, `/health` to get health information
(important when working with *k8s* or similar) and `/ws` for
websocket connection. It will not accept any incoming messages
(it will ignore them) and will just propagate to all connected
clients a message from rabbitMQ queue. There is a simple pool
of connections that handles that - it should be more reliable
and handle registering/unregistering without any hassle.
