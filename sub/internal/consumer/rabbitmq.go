package consumer

import (
	"fmt"

	"github.com/streadway/amqp"
	"github.com/wagslane/go-rabbitmq"

	"github.com/robopuff/gopubsub/sub/internal/config"
	"github.com/robopuff/gopubsub/sub/internal/logger"
)

// NewRabbitMQConsumer creates new instance of rabbitmq connection
func NewRabbitMQConsumer(cfg *config.Config) Consumer {
	return &conn{
		cfg: &cfg.RabbitMQ,
	}
}

type conn struct {
	cfg      *config.RabbitMQ
	consumer rabbitmq.Consumer
}

// Connect creates connection to rabbitMQ
// This should be refactored to be more reliable and support reconnection
func (c *conn) Connect() error {
	logger.Group("rabbitmq").WithField("address", c.cfg.Address).Info("trying to connect to rabbitmq")
	consumer, err := rabbitmq.NewConsumer(fmt.Sprintf("amqp://%s/", c.cfg.Address), amqp.Config{})
	if err != nil {
		return err
	}

	c.consumer = consumer
	return nil
}

// Close closes connection to rabbitmq
func (c *conn) Close() {
	c.consumer.Disconnect()
}

// Consume read from queue and execute callback
func (c *conn) Consume(f func(d rabbitmq.Delivery) bool) error {
	return c.consumer.StartConsuming(
		f,
		c.cfg.Queue,
		[]string{c.cfg.Queue},
	)
}
