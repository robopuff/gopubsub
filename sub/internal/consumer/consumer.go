package consumer

import (
	"github.com/wagslane/go-rabbitmq"
)

// Consumer rabbitMQ connection interface
type Consumer interface {
	// Connect creates connection
	Connect() error

	// Consume read from queue and execute callback
	Consume(func(d rabbitmq.Delivery) bool) error

	// Close closes connection
	Close()
}
