package config

import (
	"fmt"

	"github.com/ilyakaznacheev/cleanenv"

	"github.com/robopuff/gopubsub/sub/internal/logger"
)

type Config struct {
	Http Http
	RabbitMQ RabbitMQ
}

type Http struct {
	Bind int `env:"HTTP_BIND" env-default:"8090"`
}

type RabbitMQ struct {
	Address string `env:"RABBITMQ_ADDRESS" env-default:"localhost:5672"`
	Queue string `env:"RABBITMQ_QUEUE" env-default:"pubsubdemo"`
}

// Load get configuration from env or file to the struct
// This should be exported to separate library so we
// don't have to copy it
func Load() (*Config, error) {
	cfg := &Config{}
	err := cleanenv.ReadConfig(".env", cfg)
	if err == nil {
		return cfg, nil
	}

	logger.Group("config").WithError(err).Info("reading config file `.env` failed, continuing with env variables")
	if err := cleanenv.ReadEnv(cfg); err != nil {
		return cfg, fmt.Errorf("error reading environment variables: %v", err)
	}

	return cfg, nil
}