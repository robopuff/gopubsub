package ws

import (
	"math/rand"
	"time"

	"github.com/gorilla/websocket"

	"github.com/robopuff/gopubsub/sub/internal/logger"
)

type client struct {
	id []byte
	p *Pool
	ws *websocket.Conn
	send chan []byte
}

// NewClient creates new client and registers it in a pool
func NewClient(p *Pool, ws *websocket.Conn) *client {
	r := []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, 12)
	for i := range b {
		b[i] = r[rand.Intn(len(r))]
	}

	c := &client{[]byte(string(b)), p, ws, make(chan []byte)}
	p.Register(c)
	return c
}

// Writer creates writer for that client
func (c *client) Writer() {
	t := time.NewTicker(5 * time.Second)
	defer func() {
		t.Stop()
		c.ws.Close()
		c.p.Unregister(c)
	}()

	writeTimeout := 1 * time.Second
	for {
		select {
		case m, ok := <- c.send:
			c.ws.SetWriteDeadline(time.Now().Add(writeTimeout))
			if !ok {
				c.ws.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			if string(m) == "id" {
				m = c.id
			}

			if err := c.ws.WriteMessage(websocket.TextMessage, m); err != nil {
				logger.Group("pool-client").WithError(err).Error("cannot write message to client")
				return
			}
		case <- t.C:
			c.ws.SetWriteDeadline(time.Now().Add(writeTimeout))
			if err := c.ws.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}
