package ws

import (
	"github.com/robopuff/gopubsub/sub/internal/logger"
)

type Pool struct {
	clients map[*client]struct{}
}

func NewPool() *Pool {
	return &Pool{make(map[*client]struct{})}
}

func (p *Pool) Register(c *client) {
	p.clients[c] = struct{}{}
	logger.Group("pool").Infof("registered new client (%d)", len(p.clients))
}

func (p *Pool) Unregister(c *client) {
	delete(p.clients, c)
	logger.Group("pool").Infof("unregistered client (%d)", len(p.clients))
}

func (p *Pool) PropagateMessage(message []byte) {
	for client := range p.clients {
		select {
		case client.send <- message:
			logger.Group("pool").Info("propagated message")
		default:
			close(client.send)
			p.Unregister(client)
		}
	}
}