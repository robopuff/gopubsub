package server

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/robopuff/gopubsub/sub/internal/config"
)

// ErrEmptyConfig will be raised when empty configuration specified
var ErrEmptyConfig = errors.New("server cannot be instantiated with empty config")

// NewServer creates new http server with default settings
func NewServer(cfg *config.Config, handler http.Handler) (*http.Server, error) {
	if cfg == nil {
		return nil, ErrEmptyConfig
	}

	return &http.Server{
		Addr:         fmt.Sprintf(":%d", cfg.Http.Bind),
		Handler:      handler,
		ErrorLog:     log.New(os.Stderr, "http: ", log.LstdFlags),
	}, nil
}
