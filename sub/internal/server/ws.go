package server

import (
	"net/http"

	"github.com/gorilla/websocket"

	"github.com/robopuff/gopubsub/sub/internal/consumer"
	"github.com/robopuff/gopubsub/sub/internal/logger"
	"github.com/robopuff/gopubsub/sub/internal/ws"
)

type wsHandler struct {
	rabbitMQ consumer.Consumer
	pool *ws.Pool
}

// NewWebsocketHandler create new instance of the handler
func NewWebsocketHandler(rmq consumer.Consumer, p *ws.Pool) *wsHandler {
	return &wsHandler{rmq, p}
}

// Handle establishes websocket connection
func (h *wsHandler) Handle(w http.ResponseWriter, r *http.Request) {
	u := websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(*http.Request) bool {
			// don't check origin - not safe but good enough for it right now
			return true
		},
		Error: func(w http.ResponseWriter, r *http.Request, status int, reason error) {
			logger.Group("websocket").WithError(reason).Errorf("websocket error %d", status)
		},
	}
	c, err := u.Upgrade(w, r, w.Header())
	if err != nil {
		logger.Group("websocket").WithError(err).Error("websocket upgrade errored")
		http.Error(w, "could not open websocket connection", http.StatusBadRequest)
		return
	}

	client := ws.NewClient(h.pool, c)
	go client.Writer()
}