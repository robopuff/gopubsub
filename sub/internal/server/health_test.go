package server

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHandlerSetsVersionAndHashCorrectly(t *testing.T) {
	version := "my-version"
	hash := "123sad123"
	h := NewHealthHandler(version, hash)

	assert.Equal(t, h.version, version)
	assert.Equal(t, h.hash, hash)
}
