package server

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/robopuff/gopubsub/sub/internal/config"
)

type mockHandler struct{}

func (*mockHandler) ServeHTTP(_ http.ResponseWriter, _ *http.Request) {}

func TestServerTimeoutValuesAreNotZero(t *testing.T) {
	cfg := &config.Config{
		Http: config.Http{
			Bind: 1336,
		},
	}

	mh := &mockHandler{}
	srv, _ := NewServer(cfg, &mockHandler{})
	assert.Equal(t, srv.Handler, mh)
	assert.Equal(t, srv.Addr, ":1336")
}

func TestServerNilConfigErrors(t *testing.T) {
	_, err := NewServer(nil, &mockHandler{})

	assert.EqualError(t, err, ErrEmptyConfig.Error())
}
