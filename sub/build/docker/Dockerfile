# Builder
FROM golang:1.16-alpine as builder

ARG HASH=undefined
ARG VERSION=latest

ADD . /code
WORKDIR /code
RUN CGO_ENABLED=0 GOOS=linux go build \
    -ldflags "-extldflags -static -X main.hash=${HASH} -X main.version=${VERSION}" \
    -o server ./cmd/server/

# Final stage
FROM alpine:3.13.2

WORKDIR /app
COPY --from=builder /code/server .

ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.5.0/wait /wait
RUN chmod +x /wait

EXPOSE 8080
ENTRYPOINT ["./server"]
