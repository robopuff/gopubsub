module github.com/robopuff/gopubsub/sub

go 1.16

require (
	github.com/NeowayLabs/wabbit v0.0.0-20201021105516-ded4a9ef19d2
	github.com/gorilla/websocket v1.4.2
	github.com/ilyakaznacheev/cleanenv v1.2.5
	github.com/sirupsen/logrus v1.8.1
	github.com/streadway/amqp v1.0.0
	github.com/stretchr/testify v1.2.2
	github.com/wagslane/go-rabbitmq v0.6.2
)
