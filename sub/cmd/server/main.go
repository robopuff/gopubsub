package main

import (
	"net/http"

	"github.com/wagslane/go-rabbitmq"

	"github.com/robopuff/gopubsub/sub/internal/config"
	"github.com/robopuff/gopubsub/sub/internal/consumer"
	"github.com/robopuff/gopubsub/sub/internal/logger"
	app "github.com/robopuff/gopubsub/sub/internal/server"
	"github.com/robopuff/gopubsub/sub/internal/ws"
)

var hash, version string

func main() {
	cfg, err := config.Load()
	if err != nil {
		logger.Group("server").WithError(err).Panic("cannot load config")
	}

	rmq := consumer.NewRabbitMQConsumer(cfg)
	if err = rmq.Connect(); err != nil {
		logger.Group("server").WithError(err).Panic("cannot establish connection to rabbitmq")
	}
	defer rmq.Close()

	pool := ws.NewPool()
	wsh := app.NewWebsocketHandler(rmq, pool)
	if err = rmq.Consume(func(d rabbitmq.Delivery) bool {
		logger.Group("consumer").WithField("body", string(d.Body)).Info("consumed by function")
		pool.PropagateMessage(d.Body)
		// Ack
		return true
	}); err != nil {
		logger.Group("server").WithError(err).Panic("cannot consume rabbitmq messages")
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/health", app.NewHealthHandler(version, hash).Handle)
	mux.HandleFunc("/ws", wsh.Handle)

	srv, err := app.NewServer(cfg, mux)
	if err != nil {
		logger.Group("server").WithError(err).Panic("cannot create new HTTP server")
	}

	logger.Group("server").Infof("starting http server at :%d", cfg.Http.Bind)
	err = srv.ListenAndServe()
	logger.Group("server").WithError(err).Panic("error listening http server")
}
