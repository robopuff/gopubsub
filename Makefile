HASH ?= $(shell git rev-parse --short HEAD)
VERSION ?= $(shell git describe --tags 2>/dev/null)
PROJECT_NAME ?= $(shell basename "$(PWD)")
BUILDOUT ?= $(PROJECT_NAME)

all:: help

help ::
	@grep -E '^[a-zA-Z_-]+\s*:.*?## .*$$' ${MAKEFILE_LIST} | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}'

run :: # Run service
	@echo "  > Starting service"
	@docker-compose up -d pub sub
	@sleep 3
	@cd client && go run cmd/client/main.go

build :: ## Build docker images
	@echo "  > Building docker images"
	@docker-compose down
	@docker-compose build pub sub