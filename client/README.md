# Demo client

Non-reliable demo client that connects to `pub` and `sub`
services. `cmd/client/main.go` contains way too much code
and should be refactored to something more maintainable.

## Running

Run it after starting both `pub` and `sub`

```shell
$ make run
```

For the best results, use root `Makefile` and check [README.md](../README.md)

## Config (dotenv)

```dotenv
PUBLISHER=ws://localhost:8080/ws
CONSUMER=ws://localhost:8090/ws
```

## Design choices

### Libraries

* `gorilla/websocket` for easier and more compliant websockets
* `ilyakaznacheev/cleanenv` for config
* `github.com/rivo/tview` for terminal ui
