package config

import (
	"fmt"

	"github.com/ilyakaznacheev/cleanenv"
)

type Config struct {
	Publisher string `env:"PUBLISHER" env-default:"ws://localhost:8080/ws"`
	Consumer string `env:"CONSUMER" env-default:"ws://localhost:8090/ws"`
}

// Load get configuration from env or file to the struct
// This should be exported to separate library so we
// don't have to copy it
func Load() (*Config, error) {
	cfg := &Config{}
	err := cleanenv.ReadConfig(".env", cfg)
	if err == nil {
		return cfg, nil
	}

	if err := cleanenv.ReadEnv(cfg); err != nil {
		return cfg, fmt.Errorf("error reading environment variables: %v", err)
	}

	return cfg, nil
}