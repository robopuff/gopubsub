package publisher

import (
	"github.com/gorilla/websocket"
)

type Publisher interface {
	Connect() error
	Send(message string) error
}

type publisher struct {
	url  string
	conn *websocket.Conn
}

func New(url string) Publisher {
	if url == "" {
		panic("no url provided for publisher")
	}
	return &publisher{
		url: url,
	}
}

func (p *publisher) Connect() error {
	conn, _, err := websocket.DefaultDialer.Dial(p.url, nil)
	if err != nil {
		return nil
	}
	p.conn = conn
	return nil
}

func (p *publisher) Send(message string) error {
	if p.conn == nil {
		if err := p.Connect(); err != nil {
			return err
		}
	}

	return p.conn.WriteMessage(websocket.TextMessage, []byte(message))
}
