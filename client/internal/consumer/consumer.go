package consumer

import (
	"github.com/gorilla/websocket"
)

type Consumer interface {
	Connect() error
	Read() (string, error)
}

type consumer struct {
	url  string
	conn *websocket.Conn
}

func New(url string) Consumer {
	if url == "" {
		panic("no url provided for publisher")
	}
	return &consumer{
		url: url,
	}
}

func (c *consumer) Connect() error {
	conn, _, err := websocket.DefaultDialer.Dial(c.url, nil)
	if err != nil {
		return nil
	}
	c.conn = conn
	return nil
}

func (c *consumer) Read() (string, error) {
	if c.conn == nil {
		if err := c.Connect(); err != nil {
			return "", err
		}
	}

	_, message, err := c.conn.ReadMessage()
	return string(message), err
}
